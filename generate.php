<style type="text/css">
    * {
        -webkit-text-size-adjust: none;
        -webkit-text-resize: 100%;
        text-resize: 100%;
    }
</style>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td style="text-align: left;font-family: Arial, sans-serif;font-size: 12px !important;line-height: 15px !important;color:#000000;width: 100%;" align="left">
            <strong><?php print $_POST['name'] ?></strong><?php if ($_POST['title']) { print "&nbsp;".$_POST['title']; } ?><br/><br/>
            <?php if ($_POST['mobile']) { ?><span style="color:#EC008C;">M.</span> <span style="color:#000000"><?php print $_POST['mobile'] ?></span>&nbsp;&nbsp;<?php } ?><span style="color:#EC008C;">P.</span> <span style="color:#000000">(03) 9938 6666</span><br/>
            <span style="color:#EC008C;">MEL.</span> <span style="color:#000000">Area 6, 560 Church St, Richmond VIC 3121</span><br/>
            <span style="color:#EC008C;">SYD.</span> <span style="color:#000000">Level 2. 216-218 Crown St, Darlinghurst NSW 2010</span>
        </td>
    </tr>
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td><a href="http://www.cassette.com.au/?utm_source=Email+Signature+Logo&utm_medium=Email&utm_content=<?php print urlencode($_POST['name']); ?>&utm_campaign=Email+Signature" target="_blank"><img src="http://www.cassette.com.au/assets/cassette-logo.jpg" width="106" height="22" alt="Cassette" border="0" style="display: block;"/></a></td>
    </tr>
    <tr>
        <td height="15"></td>
    </tr>
    <tr>
        <td style="width: 600px;text-align: left;font-family: Georgia, Times, 'Times New Roman', serif;font-size: 14px !important;line-height: 12px !important;color:#EC008C;font-style: italic;">Sydney Office Now Open <a href="http://www.cassette.com.au/?utm_source=Email+Signature&utm_medium=Email&utm_content=<?php print urlencode($_POST['name']); ?>&utm_campaign=Email+Signature" target="_blank" style="text-decoration: underline;color:#EC008C;"><span style="color:#EC008C;">cassette.com.au</span></a>        </td>
    </tr>
    <tr>
        <td height="20"></td>
    </tr>
    <tr>
        <td style="width: 600px;text-align: left;font-family: Arial, sans-serif;font-size: 10px !important;line-height: 13px !important;color:#b5b5b5;border-top:1px solid #e8e9ea;padding-top:10px;">
            This email and any attachments are confidential. They may contain legally privileged
            information or copyright material. You should not read, copy, use or disclose them without
            authorisation. If you are not an intended recipient, please contact us at once by return email
            and then permanently delete both messages.
        </td>
    </tr>
</table>

