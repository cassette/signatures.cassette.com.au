<?php
/**
 * Document   : [signature]
 * Created on : [16/11/2010, 2:19:13 PM]
 * @author [Cassette]
 * @copyright [2010]
 * Software on this site is copyright 2010
 * Cassette Pty Ltd.
 * All Rights Reserved. Copying, Editing or
 * Distributing this software is strictly
 * forbidden without permission from
 * Cassette Pty Ltd.
 * for more information please contact
 * Cassette www.cassette.cc
 */
?>
<?php if (empty($_POST['name']) && empty($_POST['title'])){       
    ?>
<h1><img src="http://www.cassette.cc/private/signatures/1110/cassette.jpg" width="132" height="26" alt="cassette logo"/> <br/>Signature Generator</h1>
<strong>Instructions : </strong><Br/>
1. Change your default font to 'Arial regular'size 12pt (see <a href="http://email.about.com/od/macosxmailtips/qt/et_default_font.htm" target="_blank">here</a> for instructions)<br/>
2. Enter your details below.<br/>
3. Copy - paste the generated page into your signature (see <a href="http://docs.info.apple.com/article.html?artnum=61446" target="_blank">here</a> for instructions)<br/>

<br/><br/>
<form method="post" action="signature.php">
    <table cellpadding="0" cellspacing="10">
        <tr>
            <td>Full name : </td>
            <td><input type="text" id="name" name="name" /></td>
        </tr>
        <tr>
            <td>Title : </td>
            <td><input type="text" id="title" name="title"/></td>
        </tr>
        <tr>
            <td>Mobile :</td>
            <td><input type="text" id="mobile" name="mobile" /><span style="color:#ccc;font-size: 11pt;"> (optional - format : 000 000 000)</span></td>
        </tr>
        <tr><td colspan="2" ><input type="submit" value="Submit"/> </td></tr>
    </table>
</form>
<?php } ?>
<?php if (!empty($_POST['name']) && !empty($_POST['title']))  {
    ?>
        <p  style="font-family: Arial, sans-serif;font-size: 10pt">Regards,</p>
        <p>
            <span style="font-size: 11pt;font-weight: bold;font-family: Arial, sans-serif;"><?php echo $_POST['name'] ?></span><br/>
            <span style="color:rgb(140,140,140);font-size: 10pt;font-family: Arial, sans-serif;"><?php echo $_POST['title'] ?></span>
    </p>
    <table cellpadding="0" cellspacing="0" style="font-size: 10pt;color:rgb(140,140,140);font-family: Arial, sans-serif;">
        <tr>
            <td align="left"  style="padding:0px 10px 0px 0px">Phone</td><td align="left"><span style="color:rgb(0, 170,172)">(03) 9938 6666</span></td>
        </tr>
        <tr>
            <td align="left" style="padding:0px 10px 0px 0px">Fax</td><td align="left"><span style="color:rgb(0, 170,172)">(03) 9827 8454</span></td>
        </tr>
       <?php if ($_POST['mobile'] != "") {?> <tr>
            <td align="left" style="padding:0px 10px 0px 0px"> Mobile</td><td align="left"><span style="color:rgb(0, 170,172)"><?php echo $_POST['mobile'] ?></span></td>
        </tr> <?php } ?>
    </table>
    <p>
    <img src="http://www.cassette.cc/private/signatures/1110/cassette.jpg" width="132" height="26" alt="cassette logo"/>
    </p>
    <p style="color:rgb(140,140,140);font-family: Arial, sans-serif;font-size: 10pt;">
        Level 2, 299 Toorak Road,<br/>
        South Yarra, Australia 3141<br/>
        <a href="http://www.cassette.cc" style="text-decoration: none;color:rgb(140,140,140);">www.cassette.cc</a><br/>
        <a href="http://www.cassetteblog.tumblr.com/" style="text-decoration: none;"><img src="http://www.cassette.cc/private/signatures/1110/tumblr.jpg" alt="Cassette blog" border="0"  style="padding-bottom:8px;border: 0px;" width="20" height="20"></a>
        <a href="http://www.facebook.com/group.php?gid=27552322413&ref=ts" style="text-decoration: none;"><img src="http://www.cassette.cc/private/signatures/1110/facebook.jpg" alt="Cassette Facebook page"  border="0"  style="padding-bottom:8px;border: 0px" width="20" height="20"></a>
        <a href="http://twitter.com/cassettetweet" style="text-decoration: none;"><img src="http://www.cassette.cc/private/signatures/1110/twitter.jpg" alt="Follow Cassette on Twitter"  border="0" style="padding-bottom:8px;border: 0px" width="20" height="20"></a>
    <a href="http://www.cassette.cc/index.php?nav=carbon-neutral" target="_blank" style="text-decoration: none;"><img src="http://www.cassette.cc/private/signatures/1110/carbon.jpg" alt="Cassette is Carbon Neutral"  border="0" width="69" height="41"></a>
    </p>
    <p style="color:rgb(0,170,172);font-family: Arial, sans-serif;font-size: 10pt;">We will be closing for a well earned<span style="color:rgb(241,64,169)"> New Year break</span> at the<br/>
          COB <span style="color:rgb(241,64,169)">Thursday 23rd Dec</span> and reopen on<span style="color:rgb(241,64,169)"> Wednesday 5th Jan.</span></p>
    <p style="color:rgb(140,140,140);font-size: 8pt;font-family: Arial, sans-serif;">
        <span style="color:rgb(0,170,172);">Your Print and Creative projects with us have a Zero Carbon Footprint.</span><br/>
        This email and any attachments are confidential. They may contain legally<br/> privileged
        information or copyright material. You should not read, copy,<br/> use or disclose them without
        authorisation. If you are not an intended <br/>recipient, please contact us at once by return email
        and then<br/> permanently delete both messages.
    </p>
    <?php }  else if (isset($_POST['name'])) {
                    echo "<span style='color:red;'>Name and position fields are required</span>";

            }
       ?>

