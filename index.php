<?php
/**
 * Document   : [signature]
 * Created on : [16/11/2010, 2:19:13 PM]
 * @author [Cassette]
 * @copyright [2010]
 * Software on this site is copyright 2010
 * Cassette Pty Ltd.
 * All Rights Reserved. Copying, Editing or
 * Distributing this software is strictly
 * forbidden without permission from
 * Cassette Pty Ltd.
 * for more information please contact
 * Cassette www.cassette.cc
 */
?>
<h1><img src="http://www.cassette.com.au/assets/cassette-logo.jpg" alt="cassette logo"/><br/><br/>Signature Generator</h1>
<strong>Instructions : </strong><Br/>
1. Enter your details below.<br/>
2. Copy - paste the generated signature into email client (see <a href="http://www.google.com.au" target="_blank">here</a> for instructions)<br/>
<br/><br/>
<form method="post" action="generate.php">
    <table cellpadding="0" cellspacing="10">
        <tr>
            <td>Full name : </td>
            <td><input type="text" id="name" name="name" value="<?php print $_POST['name'] ?>"/></td>
        </tr>
        <tr>
            <td>Title : </td>
            <td><input type="text" id="title" name="title" value="<?php print $_POST['title'] ?>"/></td>
        </tr>
        <tr>
            <td>Mobile :</td>
            <td><input type="text" id="mobile" name="mobile" value="<?php print $_POST['mobile'] ?>"/><span style="color:#ccc;font-size: 11pt;"> (optional - format : 000 000 000)</span></td>
        </tr>
        <tr><td colspan="2" ><input type="submit" value="Submit"/> </td></tr>
    </table>
</form>

